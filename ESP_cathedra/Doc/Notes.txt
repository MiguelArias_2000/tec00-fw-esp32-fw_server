"state": {
"reported": {
    		"project": "unal_",
   	     "serial_number": "xxx",
		"report": {
			"room_temp": "xxx", 
			"door_status": "open/closed",
			"heater_status": "on/off",
			"fan_status": "on/off",
			"fan_speed": "xxxx", //RPM
			"alarm_status": "on/off",
			"error_code": "0xyyyyyyyy" // error code in hexadecimal
		}
}
}

--------------------------------------------------------------------------
--------------------------------------------------------------------------

PUBLISH:    $aws/things/unal_1053837460/shadow/update
SUBSCRIBE:  unal_1053837460/requests
SUBSCRIBE:  $aws/things/unal_1053837460/shadow/accepted

PAYLOAD (device to server):
{
  "state": {
    "reported": {
      "project": "unal_",
      "serial_number": "1053837460",
      "report": {
        "room_temp": "25",
        "door_status": "open",
        "heater_status": "on",
        "fan_status": "on",
        "fan_speed": "200",
        "alarm_status": "off",
        "error_code": "0x00000000"
      }
    }
  }
}

{
  "state": {
    "reported": {
      "project": "unal_",
      "serial_number": "1053837460",
      "fw": "v1.0"
    }
  }
}
